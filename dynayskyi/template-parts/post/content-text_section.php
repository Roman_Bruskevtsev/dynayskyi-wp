<?php 
$width = !get_sub_field('section_width') ? 'col-lg-12' : 'col-lg-8';
switch ( get_sub_field('section_align') ) {
	case '0':
		$align = ' justify-content-start';
		break;
	case '1':
		$align = ' justify-content-center';
		break;
	case '2':
		$align = ' justify-content-end';
		break;
	default:
		$align = '';
		break;
} ?>
<div class="row">
	<div class="col">
		<div class="dns-text__section">
			<div class="row<?php echo $align; ?>">
				<div class="<?php echo $width; ?>">
					<div class="text">
						<?php if( get_sub_field('title') ) { ?>
						<div class="title">
							<<?php the_sub_field('title_size'); ?>><?php the_sub_field('title'); ?></<?php the_sub_field('title_size'); ?>>
						</div>
						<?php } 
						the_sub_field('text'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
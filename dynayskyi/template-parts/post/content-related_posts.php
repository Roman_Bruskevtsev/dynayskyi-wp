<section class="dns-related__posts__section orange__background">
	<div class="container">
		<div class="row">
			<div class="col-6 col-md-8">
				<div class="dns-section__title">
					<h2><?php _e('More posts', 'dynayskyi'); ?></h2>
				</div>
			</div>
			<div class="col-6 col-md-4">
				<a class="btn btn__primary float-end" href="<?php echo get_permalink( get_option('page_for_posts') ); ?>"><?php _e('all posts', 'dynayskyi'); ?></a>	
			</div>
		</div>
		<?php 
		$tags = get_tags();
		if( $tags ) { ?>
		<div class="row">
			<div class="col">
				<div class="dns-blog__tags">
					<?php foreach ( $tags as $tag ) { 
						$tag_link = get_tag_link( $tag->term_id ); ?>
						<a href="<?php echo $tag_link; ?>"><?php echo $tag->name; ?></a>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } 
		$args = array(
			'posts_per_page'	=> 3,
			'post_type'		 	=> 'post',
			'post__not_in'		=> [get_the_ID()]
		);
		$query = new WP_Query( $args ); 
		if ( $query->have_posts() ) { ?>
		<div class="row">
			<div class="col">
				<div class="dns-posts__row d-none d-lg-block">
					<div class="row">
						<?php while ( $query->have_posts() ) { $query->the_post(); ?>
						<div class="col-md-4">
							<?php get_template_part( 'template-parts/post/content', 'post' ); ?>
						</div>
						<?php } ?>
					</div>
				</div>
				<div class="dns-posts__slider overflow-hidden d-block d-lg-none">
					<div class="swiper-wrapper">
						<?php while ( $query->have_posts() ) { $query->the_post(); ?>
						<div class="swiper-slide">
							<?php get_template_part( 'template-parts/post/content', 'post' ); ?>
						</div>
						<?php } ?>
					</div>
					<div class="swiper-pagination"></div>
				</div>
			</div>
		</div>
		<?php } wp_reset_postdata(); ?>
	</div>
</section>
<div class="row">
	<div class="col">
		<div class="dns-text__logos__section__quoter">
			<div class="row">
				<div class="col-lg-9">
					<div class="text">
						<?php if( get_sub_field('title') ) { ?>
						<div class="title">
							<<?php the_sub_field('title_size'); ?>><?php the_sub_field('title'); ?></<?php the_sub_field('title_size'); ?>>
						</div>
						<?php } 
						the_sub_field('text'); ?>
					</div>
				</div>
				<?php if( get_sub_field('logos') ) { ?>
				<div class="col-lg-3">
					<div class="image text-center">
						<img src="<?php echo get_sub_field('logos')['url']; ?>" alt="<?php echo get_sub_field('logos')['title']; ?>">
						<?php if( get_sub_field('logos_description') ) { ?>
							<p><?php the_sub_field('logos_description'); ?></p>
						<?php } ?>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
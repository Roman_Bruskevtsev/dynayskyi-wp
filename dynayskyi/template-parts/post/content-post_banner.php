<?php 
$background = get_the_post_thumbnail_url() ? ' style="background-image: url('.get_the_post_thumbnail_url().')"' : '';
$tags = get_the_tags( get_the_ID() ); ?>
    
<section class="dns-post__banner<?php echo get_the_post_thumbnail_url() ? ' background' : ''; ?>"<?php echo $background; ?>>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="content">
                    <div class="post__title text-center">
                        <h1><?php the_title(); ?></h1>
                        <div class="details">
                            <?php if( $tags ) { ?>
                            <div class="tags">
                                <?php foreach( $tags as $tag ) { ?>
                                    <div class="tag"><?php echo $tag->name; ?></div>
                                <?php } ?>
                            </div>
                            <?php } ?>
                            <div class="date"><?php echo get_the_date(); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
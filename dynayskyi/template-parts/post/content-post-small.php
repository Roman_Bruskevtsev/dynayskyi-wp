<?php 
$background = get_the_post_thumbnail_url() ? ' style="background-image: url('.get_the_post_thumbnail_url().')"' : '';
$tags = get_the_tags( get_the_ID() ); ?>
<a class="dns-post__block small" href="<?php the_permalink(); ?>">
	<div class="title">
		<h3><?php the_title(); ?></h3>
	</div>
	<div class="thumbnail"<?php echo $background; ?>></div>
	<div class="details">
		<?php if( $tags ) { ?>
		<div class="tags">
			<?php foreach( $tags as $tag ) { ?>
				<div class="tag"><?php echo $tag->name; ?></div>
			<?php } ?>
		</div>
		<?php } ?>
		<button type="button" class="btn btn__secondary float-end"><?php _e('Read', 'dynayskyi'); ?></button>
	</div>
</a>
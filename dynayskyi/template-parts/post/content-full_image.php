<?php if( get_sub_field('image') ) { 
	$width = !get_sub_field('section_width') ? 'col-lg-12' : 'col-lg-8';
	switch ( get_sub_field('section_align') ) {
		case '0':
			$align = ' justify-content-start';
			break;
		case '1':
			$align = ' justify-content-center';
			break;
		case '2':
			$align = ' justify-content-end';
			break;
		default:
			$align = '';
			break;
	} ?>
<div class="row<?php echo $align; ?>">
	<div class="<?php echo $width; ?>">
		<div class="dns-full__image">
			<img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>">
		</div>
	</div>
</div>
<?php } ?>
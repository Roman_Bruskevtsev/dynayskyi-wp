<?php
$title_color = get_sub_field('title_color') == '0' ? ' green' : ' yellow';
?>
<div class="row">
	<div class="col">
		<div class="dns-image__text__section__half">
			<div class="row">
				<?php if( get_sub_field('image') ) { ?>
				<div class="col-lg-6">
					<div class="image">
						<?php if( get_sub_field('title') && get_sub_field('title_position') == '0' ) { ?>
						<div class="title<?php echo $title_color; ?>">
							<<?php the_sub_field('title_size'); ?>><?php the_sub_field('title'); ?></<?php the_sub_field('title_size'); ?>>
						</div>
						<?php } ?>
						<img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>">
					</div>
				</div>
				<?php } ?>
				<div class="col-lg-6">
					<div class="text">
						<?php if( get_sub_field('title') && get_sub_field('title_position') == '1' ) { ?>
						<div class="title<?php echo $title_color; ?>">
							<<?php the_sub_field('title_size'); ?>><?php the_sub_field('title'); ?></<?php the_sub_field('title_size'); ?>>
						</div>
						<?php } 
						the_sub_field('text'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
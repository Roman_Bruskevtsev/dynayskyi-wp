<div class="dns-popup">
	<div class="dns-popup__close"></div>
	<?php if( get_field('form_title', 'option') ) { ?>
	<div class="dns-popup__title text-center">
		<h3><?php the_field('form_title', 'option'); ?></h3>
	</div>
	<?php } ?>
	<div class="dns-popup__body">
		<?php if( get_field('form_shortcode', 'option') ) { ?>
		<div class="dns-popup__form"><?php echo do_shortcode(get_field('form_shortcode', 'option')); ?></div>
		<?php } ?>
	</div>
</div>
<section class="dns-hero__section">
	<?php if( get_sub_field('slider') ) { ?>
	<div class="dns-hero__slider">
		<div class="swiper-wrapper">
			<?php foreach ( get_sub_field('slider') as $slide ) { 
				$background = $slide['image'] ? ' style="background-image: url('.$slide['image'].')"' : ''; ?>
				<div class="swiper-slide"<?php echo $background; ?>>
					<?php if( $slide['title'] ) { ?>
					<div class="content">
						<div class="container">
							<div class="row">
								<div class="col-lg-7">
									<h2><?php echo $slide['title']; ?></h2>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
		<div class="swiper-pagination"></div>
	</div>
	<div class="dns-scroll__icon d-none d-lg-block"></div>
	<?php } ?>
</section>
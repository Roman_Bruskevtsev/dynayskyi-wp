<section class="dns-about__company__section scroll__section"<?php echo get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"': ''; ?>>
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="dns-section__title">
					<h3><?php the_sub_field('title'); ?></h3>
				</div>
			</div>
		</div>
		<?php } 
		if( get_sub_field('blocks') ) { ?>
		<div class="row">
			<div class="col">
				<div class="dns-company__blocks d-none d-lg-block">
					<div class="row">
						<?php foreach ( get_sub_field('blocks') as $block ) { ?>
						<div class="col-md-4">
							<div class="dns-company__block text-center">
								<?php if( $block['icon'] ) { ?>
									<div class="icon">
										<img src="<?php echo $block['icon']['url']; ?>" alt="<?php echo $block['icon']['title']; ?>">
									</div>
								<?php } 
								if( $block['text'] ) { ?>
									<div class="text"><?php echo $block['text']; ?></div>
								<?php } ?>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
				<div class="dns-company__blocks__slider overflow-hidden d-block d-lg-none">
					<div class="swiper-wrapper">
						<?php foreach ( get_sub_field('blocks') as $block ) { ?>
						<div class="swiper-slide">
							<div class="dns-company__block text-center">
								<?php if( $block['icon'] ) { ?>
									<div class="icon">
										<img src="<?php echo $block['icon']['url']; ?>" alt="<?php echo $block['icon']['title']; ?>">
									</div>
								<?php } 
								if( $block['text'] ) { ?>
									<div class="text"><?php echo $block['text']; ?></div>
								<?php } ?>
							</div>
						</div>
						<?php } ?>
					</div>
					<div class="swiper-pagination"></div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>
<?php 
$partners = get_sub_field('logos');
?>
<section class="dns-partners__section scroll__section"<?php echo get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"': ''; ?>>
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="dns-section__title margin">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
		<?php } 
		if( get_sub_field('text_1') || get_sub_field('text') ) { ?>
		<div class="row">
			<?php if( get_sub_field('text_1') ) { ?>
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-6">
						<div class="text"><?php the_sub_field('text_1'); ?></div>
					</div>
					<div class="col-lg-6">
						<?php if( get_field('facebook', 'option') || get_field('instagram', 'option') ) { ?>
                        <div class="dns-social__block green float-start">
                        <?php if( get_field('facebook', 'option') ) { ?>
                            <a class="facebook" target="_blank" href="<?php the_field('facebook', 'option'); ?>"></a>
                        <?php }
                        if( get_field('instagram', 'option') ) { ?>
                            <a class="instagram" target="_blank" href="<?php the_field('instagram', 'option'); ?>"></a>
                        <?php } ?>
                        </div>
                        <?php } ?>
					</div>
				</div>
				
			</div>
			<?php } 
			if( get_sub_field('text') ) { ?>
			<div class="col-lg-5">
				<div class="text"><?php the_sub_field('text'); ?></div>
			</div>
			<?php } ?>
		</div>
		<?php }
		if( $partners ) { ?>
		<div class="row">
			<div class="col">
				<div class="dns-partners__list d-none d-sm-block">
					<div class="row">
						<?php foreach( $partners as $partner ) { ?>
							<div class="col-sm-4 col-md-3 col-lg-2">
							<?php if( $partner['url'] ) { ?>
								<a href="<?php echo $partner['url']; ?>" target="_blank" class="dns-partner__block">
							<?php } else { ?>
								<div class="dns-partner__block">
							<?php } ?>
								<?php if( $partner['logo'] ) { ?>
									<img src="<?php echo $partner['logo']['url']; ?>" alt="<?php echo $partner['logo']['title']; ?>">
								<?php } ?>
							<?php if( $partner['url'] ) { ?>
								</a>
							<?php } else { ?>
								</div>
							<?php } ?>
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="dns-partners__slider swiper d-sm-none">
					<div class="swiper-wrapper">
						<?php foreach( $partners as $partner ) { ?>
						<div class="swiper-slide">
							<?php if( $partner['url'] ) { ?>
								<a href="<?php echo $partner['url']; ?>" target="_blank" class="dns-partner__block">
							<?php } else { ?>
								<div class="dns-partner__block">
							<?php } ?>
								<?php if( $partner['logo'] ) { ?>
									<img src="<?php echo $partner['logo']['url']; ?>" alt="<?php echo $partner['logo']['title']; ?>">
								<?php } ?>
							<?php if( $partner['url'] ) { ?>
								</a>
							<?php } else { ?>
								</div>
							<?php } ?>
						</div>
						<?php } ?>
					</div>
					<div class="swiper-pagination"></div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>
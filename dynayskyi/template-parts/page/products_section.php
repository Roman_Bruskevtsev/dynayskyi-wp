<section class="dns-products__section scroll__section"<?php echo get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"': ''; ?>>
	<div class="container">
		<?php if( get_sub_field('small_title') || get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="dns-section__title">
					<?php if( get_sub_field('small_title') ) { ?>
						<h3><?php the_sub_field('small_title'); ?></h3>
					<?php } 
					if( get_sub_field('title') ) { ?>
						<h2><?php the_sub_field('title'); ?></h2>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } 
		$products_categories = get_sub_field('choose_products_categories'); 
		$lang = pll_current_language();
		if( $products_categories ) { ?>
		<div class="row">
			<div class="col">
				<div class="dns-products__block">
					<div class="categories__row">
						<?php foreach( $products_categories as $category ) { ?>
						<div class="category__item" data-id="<?php echo $category->term_id; ?>" data-lang="<?php echo $lang; ?>"><?php echo $category->name; ?></div>
						<?php } ?>
						<div class="show__category d-block d-lg-none"></div>
					</div>
				
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>
<?php 
$blocks = get_sub_field('blocks');
?>
<section class="dns-our__values__section scroll__section"<?php echo get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"': ''; ?>>
	<?php if( $blocks ){ 
		$i = 1;
		foreach ( $blocks as $block ) { 
			$orange = $block['orange_block'] ? ' green__background' : '';
			$gallery = $block['gallery'];
			$class = $gallery ? ' small__padding' : '';
			?>
			<div class="dns-our__values__block<?php echo $orange.$class; ?>">
				<?php if( $i % 2 == 1 ) { ?>
				<div class="image left" style="background-image: url('<?php echo $block['image']; ?>');"></div>
				<div class="container">
					<div class="row">
						<div class="col-lg-7"></div>
						<div class="col-lg-5">
							<div class="content">
								<?php if( $block['small_title'] ) { ?>
									<h3><?php echo $block['small_title']; ?></h3>
								<?php }
								if( $block['title'] ) { ?><h2><?php echo $block['title']; ?></h2><?php }
								echo $block['text']; 
								if( $gallery ) { ?>
								<div class="dns-gallery__slider">
									<div class="swiper-wrapper">
									<?php foreach ( $gallery as $slide ) { ?>
										<div class="swiper-slide">
											<a href="<?php echo $slide['url']; ?>">
												<img src="<?php echo $slide['url']; ?>" alt="<?php echo $slide['title']; ?>">
											</a>
										</div>
									<?php } ?>
									</div>
									<div class="swiper-button-prev"></div>
									<div class="swiper-button-next"></div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<?php }
				if( $i % 2 == 0 ) { ?>
				<div class="container">
					<div class="row">
						<div class="col-lg-5">
							<div class="content">
								<?php if( $block['small_title'] ) { ?>
									<h3><?php echo $block['small_title']; ?></h3>
								<?php }
								if( $block['title'] ) { ?><h2><?php echo $block['title']; ?></h2><?php }
								echo $block['text'];
								if( $gallery ) { ?>
								<div class="dns-gallery__slider">
									<div class="swiper-wrapper">
									<?php foreach ( $gallery as $slide ) { ?>
										<div class="swiper-slide text-center">
											<a href="<?php echo $slide['url']; ?>">
												<img src="<?php echo $slide['url']; ?>" alt="<?php echo $slide['title']; ?>">
											</a>
										</div>
									<?php } ?>
									</div>
									<div class="swiper-button-prev"></div>
									<div class="swiper-button-next"></div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<div class="image right" style="background-image: url('<?php echo $block['image']; ?>');"></div>
				<?php } ?>
			</div>
		<?php $i++; } 
	} ?>
</section>
<section class="dns-blog__section orange__background scroll__section"<?php echo get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"': ''; ?>>
	<div class="container">
		<?php if( get_sub_field('small_title') || get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-6 col-md-8">
				<div class="dns-section__title">
					<?php if( get_sub_field('title') ) { ?>
						<h2><?php the_sub_field('title'); ?></h2>
					<?php } ?>
				</div>
			</div>
			<?php if( get_sub_field('button_url') || get_sub_field('button_label') ) { ?>
			<div class="col-6 col-md-4">
				<a class="btn btn__primary float-end" href="<?php the_sub_field('button_url'); ?>"><?php the_sub_field('button_label'); ?></a>	
			</div>
			<?php } ?>
		</div>
		<?php } 
		$tags = get_sub_field('tags'); 
		if( $tags ) { ?>
		<div class="row">
			<div class="col">
				<div class="dns-blog__tags">
					<?php foreach ( $tags as $tag ) { 
						$term_link = get_tag_link($tag->term_id); ?>
						<a href="<?php echo $term_link; ?>"><?php echo $tag->name; ?></a>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } 
		$args = array(
			'posts_per_page'	=> 3,
			'post_type'		 	=> 'post'
		);
		$query = new WP_Query( $args ); 
		if ( $query->have_posts() ) { ?>
		<div class="row">
			<div class="col">
				<div class="dns-posts__row d-none d-lg-block">
					<div class="row">
						<?php while ( $query->have_posts() ) { $query->the_post(); ?>
						<div class="col-md-4">
							<?php get_template_part( 'template-parts/post/content', 'post' ); ?>
						</div>
						<?php } ?>
					</div>
				</div>
				<div class="dns-posts__slider overflow-hidden d-block d-lg-none">
					<div class="swiper-wrapper">
						<?php while ( $query->have_posts() ) { $query->the_post(); ?>
						<div class="swiper-slide">
							<?php get_template_part( 'template-parts/post/content', 'post' ); ?>
						</div>
						<?php } ?>
					</div>
					<div class="swiper-pagination"></div>
				</div>
			</div>
		</div>
		<?php } wp_reset_postdata(); ?>
	</div>
</section>
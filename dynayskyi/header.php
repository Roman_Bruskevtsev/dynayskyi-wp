<?php
/**
 * @package WordPress
 * @subpackage Dynayskyi
 * @since 1.0
 * @version 1.0
 */
$languages = pll_the_languages( array('raw' => 1) );
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <header class="dns-header">
        <div class="container">
            <?php if( is_front_page() ) { ?>
            <div class="row">
                <div class="col-7 col-lg-3">
                    <?php if( get_field('logo', 'option') ) { ?>
                    <a class="dns-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo get_field('logo', 'option')['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                    <?php } ?>
                </div>
                <div class="col-5 col-lg-9">
                    <div class="dns-nav__block float-end">
                        <?php if( has_nav_menu('main') ) { ?>
                        <div class="dns-mobile__btn float-start d-block d-lg-none">
                            <span></span>
                            <span></span>
                        </div>
                        <div class="dns-main__menu float-start d-none d-lg-block">
                            <?php wp_nav_menu( array(
                                'theme_location'        => 'main',
                                'container'             => 'nav',
                                'container_class'       => 'dns-main__nav'
                            ) ); ?>
                        </div>
                        <?php }
                        if( $languages ){
                            $current_lang = $lang_list = '';
                            foreach ( $languages as $lang ) {
                                if( !$lang['current_lang'] ){
                                    $lang_list .= '<li><a href="'.$lang['url'].'">'.$lang['slug'].'</a></li>';
                                }
                            } ?>
                            <div class="dns-language__switcher float-start d-none d-lg-block">
                                <ul>
                                    <?php echo $lang_list; ?>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php } else { ?>
            <div class="row">
                <?php if( is_single() ) { ?>
                <div class="col-3 col-md-3 position-relative">
                    <a class="dns-blog__nav" href="<?php echo get_permalink( get_option('page_for_posts') ); ?>"></a>
                </div>
                <div class="col-6 col-md-6">
                    <?php if( get_field('logo', 'option') ) { ?>
                    <a class="dns-logo large text-center" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo get_field('logo', 'option')['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                    <?php } ?>
                </div>
                <?php } else { ?>
                <div class="col-9 col-md-9">
                    <?php if( get_field('logo', 'option') ) { ?>
                    <a class="dns-logo text-center float-start" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo get_field('logo', 'option')['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                    <?php } ?>
                </div>
                <?php } ?>
                <div class="col-3 col-md-3">
                    <?php if( $languages ){
                        $current_lang = $lang_list = '';
                        foreach ( $languages as $lang ) {
                            if( !$lang['current_lang'] ){
                                $lang_list .= '<li><a href="'.$lang['url'].'">'.$lang['slug'].'</a></li>';
                            }
                        } ?>
                        <div class="dns-language__switcher float-end">
                            <ul>
                                <?php echo $lang_list; ?>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </header>
    <div class="dns-mobile__wrapper d-block d-lg-none">
        <?php if( has_nav_menu('main') ) { ?>
        <div class="dns-mobile__menu">
            <?php wp_nav_menu( array(
                'theme_location'        => 'main',
                'container'             => 'nav',
                'container_class'       => 'dns-mobile__nav'
            ) ); ?>
        </div>
        <?php }
        if( $languages ){
            $current_lang = $lang_list = '';
            foreach ( $languages as $lang ) {
                if( !$lang['current_lang'] ){
                    $lang_list .= '<li><a href="'.$lang['url'].'">'.$lang['slug'].'</a></li>';
                }
            } ?>
            <div class="dns-language__switcher float-start">
                <ul>
                    <?php echo $lang_list; ?>
                </ul>
            </div>
        <?php } ?>
    </div>
    <main>
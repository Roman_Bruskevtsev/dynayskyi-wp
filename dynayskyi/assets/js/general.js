'use strict';

class GeneralClass{
    constructor(){
        this.init();
    }

    init(){
        this.documentReady();
        this.windowLoad();
    }

    documentReady(){
        document.addEventListener('DOMContentLoaded', function(){
            AOS.init();
            general.headerAnimationInit();
            general.heroSliderInit();
            general.companyBlocksSliderInit();
            general.blogSliderInit();
            general.gallerySliderInit();
            general.partnersSliderInit();
            general.mobileNavigation();
            general.productsBlock();
            general.popupBlock();
        });
    }
    windowLoad(){
        (function($) {
            $(window).on('load', function(){
                $('.dns-gallery__slider').lightGallery({
                    selector: 'a',
                    thumbnail: false
                });  
            });
        })(jQuery);

        window.onload = function() {
            general.sectionScrollInit();
        }
    }

    headerAnimationInit(){
        let header = document.querySelector('.dns-header'),
            lastScrollTop = 0,
            smoothMenuPoints = document.querySelectorAll('.dns-main__nav li a[href^="#"]'),
            smoothMobileMenuPoints = document.querySelectorAll('.dns-mobile__nav li a[href^="#"]'),
            st;

        window.addEventListener('scroll', () => {
            st = window.pageYOffset;
            
            if( st <= 0 ){
                header.classList.remove('scroll__down');
            } else if ( st > lastScrollTop && st > 0 ){
                header.classList.add('scroll__down');
            } else {
                header.classList.remove('scroll__down');
            }
            if( st > 100 ){
                header.classList.add('dark');
            } else {
                header.classList.remove('dark');
            }

            lastScrollTop = st;
        });

        if( smoothMenuPoints ){
            let headerHeight, pointHref, pointTopHeight, siteUrl, newUrl;
            smoothMenuPoints.forEach( (point) => {
                point.addEventListener('click', (e) => {
                    e.preventDefault();
                    headerHeight = header.offsetHeight;
                    pointHref = point.getAttribute('href');
                    pointTopHeight = document.querySelector(pointHref).getBoundingClientRect().top + window.scrollY - headerHeight;
                    siteUrl = document.URL.split('#')[0];
                    newUrl = siteUrl + pointHref;

                    window.history.pushState({}, '', newUrl);

                    window.scrollTo({
                        top: pointTopHeight,
                        behavior: "smooth"
                    });
                });
            });

            smoothMobileMenuPoints.forEach( (point) => {
                point.addEventListener('click', (e) => {
                    e.preventDefault();
                    headerHeight = header.offsetHeight;
                    pointHref = point.getAttribute('href');
                    pointTopHeight = document.querySelector(pointHref).getBoundingClientRect().top + window.scrollY - headerHeight;
                    siteUrl = document.URL.split('#')[0];
                    newUrl = siteUrl + pointHref;

                    window.history.pushState({}, '', newUrl);
                    
                    window.scrollTo({
                        top: pointTopHeight,
                        behavior: "smooth"
                    });
                });
            });
        }
    }

    mobileNavigation(){
        let btn = document.querySelector('.dns-mobile__btn'),
            header = document.querySelector('.dns-header'),
            menuPoints = document.querySelectorAll('.dns-mobile__nav a'),
            menu = document.querySelector('.dns-mobile__wrapper');
        if( !btn ) return false;
        btn.addEventListener('click', () => {
            btn.classList.toggle('show');
            header.classList.toggle('show');
            menu.classList.toggle('show');
        });

        menuPoints.forEach( (item) => {
            item.addEventListener('click', () => {
                menuPoints.forEach( (point) => {
                    point.classList.remove('active');
                });
                item.classList.add('active');
                header.classList.remove('show');
                btn.classList.remove('show');
                menu.classList.remove('show');
            });
        });
    }

    heroSliderInit(){
        let slider = document.querySelector('.dns-hero__slider'),
            scroll = document.querySelector('.dns-scroll__icon');
        if( !slider ) return false;
        let testimonialsSlider = new Swiper(slider, {
            slidesPerView: 1,
            speed: 700,
            effect: 'fade',
            navigation: false,
            autoplay: {
                delay: 5000,
                disableOnInteraction: false
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            },
        });
        scroll.addEventListener('click', () => {
            window.scrollTo({
                top: slider.getBoundingClientRect().height,
                behavior: "smooth"
            });
        });
    }

    companyBlocksSliderInit(){
        let slider = document.querySelector('.dns-company__blocks__slider');
        if( !slider ) return false;
        let companySlider = new Swiper(slider, {
            slidesPerView: 1,
            spaceBetween: 20,
            speed: 700,
            navigation: false,
            // autoplay: {
            //     delay: 5000,
            //     disableOnInteraction: false
            // },
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            },
            breakpoints: {
                767: {
                    slidesPerView: 2
                }
            }
        });
    }

    blogSliderInit(){
        let slider = document.querySelector('.dns-posts__slider');
        if( !slider ) return false;
        let blogSlider = new Swiper(slider, {
            slidesPerView: 1,
            spaceBetween: 20,
            speed: 700,
            navigation: false,
            // autoplay: {
            //     delay: 5000,
            //     disableOnInteraction: false
            // },
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            },
            breakpoints: {
                767: {
                    slidesPerView: 2
                }
            }
        });
    }

    gallerySliderInit(){
        let slider = document.querySelector('.dns-gallery__slider');
        if( !slider ) return false;
        let gallerySlider = new Swiper(slider, {
            slidesPerView: 1,
            speed: 700,
            noSwiping: true,
            noSwipingClass: 'dns-gallery__slider',
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            }
        });
    }

    partnersSliderInit(){
        let slider = document.querySelector('.dns-partners__slider');
        if( !slider ) return false;
        let gallerySlider = new Swiper(slider, {
            slidesPerView: 2,
            grid: {
                rows: 5,
            },
            speed: 700,
            pagination: {
                el: '.swiper-pagination',
            },
        });
    }

    sectionScrollInit(){
        let scrollSections = document.querySelectorAll('.scroll__section'),
            header = document.querySelector('.dns-header'),
            menuItems = document.querySelectorAll('.dns-main__menu li'),
            mobileMenuItems = document.querySelectorAll('.dns-mobile__menu li'),
            topScroll, sectionScroll, sectionHeight, sectionActiveHeight, activeSection, linkHref;
        if(!scrollSections) return false;

        window.addEventListener('scroll', function() {
            topScroll = window.scrollY;
            scrollSections.forEach( (section) => {
                sectionScroll = section.getBoundingClientRect().top;

                if( sectionScroll - header.offsetHeight <= 0 ) {
                    activeSection = '#' + section.getAttribute('id');

                    menuItems.forEach( (item) => {
                        linkHref = item.querySelector('a').getAttribute('href');
                        linkHref = '#' + linkHref.substring(linkHref.indexOf('#') + 1);

                        if( linkHref == activeSection ){
                            item.querySelector('a').classList.add('active');
                        } else {
                            item.querySelector('a').classList.remove('active');
                        }
                    });

                    mobileMenuItems.forEach( (item) => {
                        linkHref = item.querySelector('a').getAttribute('href');
                        linkHref = '#' + linkHref.substring(linkHref.indexOf('#') + 1);

                        if( linkHref == activeSection ){
                            item.querySelector('a').classList.add('active');
                        } else {
                            item.querySelector('a').classList.remove('active');
                        }
                    });
                }
            });
        });
    }

    productsBlock(){
        let block = document.querySelector('.dns-products__block');
        if( !block ) return false;
        let items = block.querySelectorAll('.category__item'),
            categoryBtn = document.querySelector('.show__category'),
            categoryId, categoryLang, xhr;
        if( items ) {
            items.forEach( (item) => {
                item.addEventListener('click', () => {
                    categoryId  = item.dataset.id,
                    categoryLang = item.dataset.lang,
                    xhr         = new XMLHttpRequest();

                    items.forEach( (item) => {
                        item.classList.remove('active');
                    });
                    item.classList.add('active');

                    xhr.open('POST', ajaxurl, true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;');
                    xhr.onload = function () {
                        if( block.querySelector('.products__wrapper') ) block.querySelector('.products__wrapper').remove();
                        block.insertAdjacentHTML('beforeend', this.response );
                        culturesItemsClick();
                    }
                    xhr.onerror = function() {
                        console.log('connection error');
                    };
                    xhr.send('action=load_products&category='+ categoryId + '&lang=' + categoryLang);
                });
            });
        }

        function culturesItemsClick(){
            let items = document.querySelectorAll('.products__wrapper .culture__item');
            if( items ) {
                let productsNavs = document.querySelectorAll('.products__block .nav .item'),
                    productsBlocks = document.querySelectorAll('.products__block .products .product__block'),
                    culturesBtn = document.querySelector('.show__cultures'),
                    itemsBtn = document.querySelector('.show__items');
                items.forEach( (item) => {
                    item.addEventListener('click', () => {
                        items.forEach( (item) => {
                            item.classList.remove('active');
                        });
                        item.classList.add('active');
                        let cultureId = item.dataset.cultureId,
                            navTerms;
                        productsNavs.forEach( (nav) => {
                            navTerms = nav.dataset.terms;
                            if( navTerms.includes(cultureId) ){
                                nav.classList.add('show');
                            } else {
                                nav.classList.remove('show');
                            }
                            nav.addEventListener('click', () => {
                                let productId = nav.dataset.productId;
                                productsNavs.forEach( (navItem) => {
                                    navItem.classList.remove('active');
                                });
                                nav.classList.add('active');
                                productsBlocks.forEach( (product) => {
                                    let id = product.dataset.id;
                                    if( productId == id ){
                                        product.classList.add('active');
                                    } else {
                                        product.classList.remove('active');
                                    }
                                });
                            });
                        });
                        document.querySelector('.products__block .nav .item.show').click();
                    });
                });

                if( culturesBtn ) {
                    culturesBtn.addEventListener('click', () => {
                        culturesBtn.closest('.cultures__row').classList.toggle('show');
                        culturesBtn.classList.toggle('show');
                    });
                }

                if( itemsBtn ) {
                    itemsBtn.addEventListener('click', () => {
                        itemsBtn.closest('.nav').classList.toggle('visible');
                        itemsBtn.classList.toggle('show');
                    });
                }

                if(document.querySelector('.products__wrapper .culture__item')) document.querySelector('.products__wrapper .culture__item').click();
            }
        }

        if( categoryBtn ) {
            categoryBtn.addEventListener('click', () => {
                categoryBtn.closest('.categories__row').classList.toggle('show');
                categoryBtn.classList.toggle('show');
            });
        }

        document.querySelector('.dns-products__block .category__item').click();
    }

    popupBlock(){
        let popupBtn = document.querySelector('#request-popup'),
            popup = document.querySelector('.dns-popup'),
            popupClose = popup.querySelector('.dns-popup__close');

        if( !popupBtn && !popup ) return false;
        
        popupBtn.addEventListener('click', () => {
            popup.classList.add('show');
        });

        popupClose.addEventListener('click', () => {
            popup.classList.remove('show');
        });
    }
}

let general = new GeneralClass();
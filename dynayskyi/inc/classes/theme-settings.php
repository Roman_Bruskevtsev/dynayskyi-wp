<?php
/**
 * 
 */
class ThemeSettingsClass {
	const SCRIPTS_VERSION    = '1.0.12';

	public function __construct(){
		$this->scriptsDir = get_theme_file_uri().'/assets/js';
        $this->stylesDir = get_theme_file_uri().'/assets/css';

		$this->actions_init();
	}

	public function actions_init(){
		add_action( 'wp_enqueue_scripts', array( $this, 'scripts_styles' ) );
		add_action( 'after_setup_theme', array( $this, 'theme_setup' ) );
		add_action( 'wp_footer',  array( $this, 'js_variables' ) );
		add_action( 'widgets_init', array( $this, 'widgets_init') );
		add_action( 'init', array( $this, 'custom_posts_type') );
		add_action( 'init', array( $this, 'custom_taxonomy') );
		add_action( 'wp_ajax_load_products', array( $this, 'load_products' ) );
        add_action( 'wp_ajax_nopriv_load_products', array( $this, 'load_products' ) );
		
		add_filter( 'upload_mimes', array( $this, 'enable_svg_types'), 99 );
	}

	public function scripts_styles() {
		wp_enqueue_style( 'dynayskyi', $this->stylesDir.'/main.min.css' , '', self::SCRIPTS_VERSION);
    	wp_enqueue_style( 'dynayskyi-style', get_stylesheet_uri() );

    	wp_enqueue_script( 'google-maps-key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDOqauekT_r4JzPY18FOWN4N8DFR3hXo1U&libraries=places', array( 'jquery' ), self::SCRIPTS_VERSION, true );
    	wp_enqueue_script( 'particles', 'https://cdnjs.cloudflare.com/ajax/libs/particles.js/2.0.0/particles.min.js', array( 'jquery' ), '', true );
    	
    	wp_enqueue_script( 'dynayskyi', $this->scriptsDir.'/all.min.js', array( 'jquery' ), self::SCRIPTS_VERSION, true );
        
    }

    public function theme_setup(){
    	load_theme_textdomain( 'dynayskyi' );
	    add_theme_support( 'automatic-feed-links' );
	    add_theme_support( 'title-tag' );
	    add_theme_support( 'post-thumbnails' );
	    add_theme_support( 'post-formats', array( 'video', 'audio' ) );

	    add_image_size( 'service-thumbnails', 360, 160, true );
	    add_image_size( 'new-thumbnails', 360, 240, true );
	    add_image_size( 'product-thumbnails', 848, 436, true );

	    register_nav_menus( array(
	        'main'          => __( 'Main Menu', 'dynayskyi' )
	    ) );

	    if( function_exists('acf_add_options_page') ) {
		    $general = acf_add_options_page(array(
		        'page_title'    => __('Theme General Settings', 'dynayskyi'),
		        'menu_title'    => __('Theme Settings', 'dynayskyi'),
		        'redirect'      => false,
		        'capability'    => 'edit_posts',
		        'menu_slug'     => 'theme-settings',
		    ));
		}
    }

    public function widgets_init(){
    	register_sidebar( array(
	        'name'          => __( 'Footer 1', 'dynayskyi' ),
	        'id'            => 'footer-1',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'dynayskyi' ),
	        'before_widget' => '<section id="%1$s" class="widget %2$s">',
	        'after_widget'  => '</section>',
	        'before_title'  => '<h5>',
	        'after_title'   => '</h5>',
	    ) );
	    register_sidebar( array(
	        'name'          => __( 'Footer 2', 'dynayskyi' ),
	        'id'            => 'footer-2',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'dynayskyi' ),
	        'before_widget' => '<section id="%1$s" class="widget %2$s">',
	        'after_widget'  => '</section>',
	        'before_title'  => '<h5>',
	        'after_title'   => '</h5>',
	    ) );
	    register_sidebar( array(
	        'name'          => __( 'Footer 3', 'dynayskyi' ),
	        'id'            => 'footer-3',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'dynayskyi' ),
	        'before_widget' => '<section id="%1$s" class="widget %2$s">',
	        'after_widget'  => '</section>',
	        'before_title'  => '<h5>',
	        'after_title'   => '</h5>',
	    ) );
    }

    public function enable_svg_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}

	public function js_variables(){ ?>
		<script type="text/javascript">
	        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>',
	        	asyncUpload = '<?php echo admin_url('async-upload.php'); ?>';
	    </script>
	<?php }

	public function custom_posts_type(){
		$services_labels = array(
			'name'					=> __('Products', 'dynayskyi'),
			'singular_name'			=> __('Product', 'dynayskyi'),
			'add_new'				=> __('Add Product', 'dynayskyi'),
			'add_new_item'			=> __('Add New Product', 'dynayskyi'),
			'edit_item'				=> __('Edit Product', 'dynayskyi'),
			'new_item'				=> __('New Product', 'dynayskyi'),
			'view_item'				=> __('View Product', 'dynayskyi')
		);

		$services_args = array(
			'label'               => __('Products', 'dynayskyi'),
			'description'         => __('Product information page', 'dynayskyi'),
			'labels'              => $services_labels,
			'supports'            => array( 'title' ),
			'taxonomies'          => array( '' ),
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'has_archive'         => false,
			'can_export'          => true,
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => false,
			'exclude_from_search' => false,
			'query_var'           => true,
			'rewrite'             => array(
				'slug'			  => 'products'
			),
			'capability_type'     => 'post',
			'menu_position'		  => 4,
			'menu_icon'           => 'dashicons-carrot'
		);

		register_post_type( 'product', $services_args );
	}

	public function custom_taxonomy(){
		$taxonomy_labels = array(
			'name'                        => __('Products Categories','dynayskyi'),
			'singular_name'               => __('Products Category','dynayskyi'),
			'menu_name'                   => __('Products Categories','dynayskyi'),
		);

		$taxonomy_rewrite = array(
			'slug'                  => 'products-categories',
			'with_front'            => true,
			'hierarchical'          => true,
		);

		$taxonomy_args = array(
			'labels'              => $taxonomy_labels,
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'show_in_nav_menus'   => true,
			'show_tagcloud'       => true,
			'rewrite'             => $taxonomy_rewrite,
		);
		register_taxonomy( 'products-categories', 'product', $taxonomy_args );

		$taxonomy_labels = array(
			'name'                        => __('Products Cultures','dynayskyi'),
			'singular_name'               => __('Products Culture','dynayskyi'),
			'menu_name'                   => __('Products Cultures','dynayskyi'),
		);

		$taxonomy_rewrite = array(
			'slug'                  => 'products-cultures',
			'with_front'            => true,
			'hierarchical'          => true,
		);

		$taxonomy_args = array(
			'labels'              => $taxonomy_labels,
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'show_in_nav_menus'   => true,
			'show_tagcloud'       => true,
			'rewrite'             => $taxonomy_rewrite,
		);
		register_taxonomy( 'products-cultures', 'product', $taxonomy_args );
	}

	public function load_products(){
		$cat = $_POST['category'];
		$lang = $_POST['lang'];
		$args = array(
			'posts_per_page' 	=> -1,
			'post_type' 		=> 'product',
			'post_status'		=> 'publish',
			'tax_query' => array(
				array(
					'taxonomy' => 'products-categories',
					'field'    => 'term_id',
					'terms'    => $cat
				)
			)
		);
		if( $lang ) $args['lang'] = $lang;

		$cultures =	$products = [];

		$query = new WP_Query( $args );

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) { $query->the_post();
				$terms = get_the_terms( get_the_ID(), 'products-cultures' );
				$post_terms = ''; 

				foreach( $terms as $term ){
					if( !in_array( $term, $cultures ) ) $cultures[] = $term;
					if( $post_terms == '' ){
						$post_terms = $term->term_id;
					} else{
						$post_terms = $post_terms.','.$term->term_id;
					}
				}
				$products[] = [
					'id'	=> get_the_ID(),
					'name'	=> get_the_title(),
					'terms'	=> $post_terms
				];
			}
			if( $terms ){ ?>
			<div class="products__wrapper">
				<div class="row">
					<div class="col">
						<div class="cultures__row">
							<?php foreach ( $cultures as $term ) { ?>
								<div class="culture__item" data-culture-id="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></div>
							<?php } ?>
							<div class="show__cultures d-block d-lg-none"></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<div class="products__block">
							<div class="row">
								<div class="col-md-2">
									<div class="nav">
									<?php foreach ($products as $product ) { ?>
										<div class="item" data-terms="<?php echo $product['terms']; ?>" data-product-id="<?php echo $product['id']; ?>"><?php echo $product['name']; ?></div>
									<?php } ?>
										<div class="show__items d-block d-lg-none"></div>
									</div>
								</div>
			<?php }
		}

		if ( $query->have_posts() ) { ?>
								<div class="col-md-10">
									<div class="products">
									<?php while ( $query->have_posts() ) { $query->the_post(); 
										$terms = get_the_terms( get_the_ID(), 'products-cultures' ); 
										$post_terms = ''; 
										foreach( $terms as $term ){
											if( $post_terms == '' ){
												$post_terms = $term->term_id;
											} else{
												$post_terms = $post_terms.','.$term->term_id;
											}
										} ?>
										<div class="product__block" data-terms="<?php echo $post_terms; ?>" data-id="<?php the_id(); ?>">
											<div class="row">
												<div class="col-md-6">
													<div class="image">
													<?php if( get_field('title') ) { ?><h3 class="d-block d-lg-none"><?php the_field('title'); ?></h3><?php } ?>
													<?php if( get_field('image') ) { ?>
														<img src="<?php echo get_field('image')['url']; ?>" alt="<?php echo get_field('image')['title']; ?>">
													<?php } else { ?>
														<img src="<?php echo get_theme_file_uri().'/assets/images/products/no-image.png'; ?>" alt="<?php the_title(); ?>">
													<?php } ?>
													</div>
												</div>
												<div class="col-md-6">
													<div class="description">
														<?php if( get_field('title') ) { ?><h3 class="d-none d-lg-block"><?php the_field('title'); ?></h3><?php } ?>
														<?php the_field('description'); ?>		
													</div>
												</div>
											</div>
										</div>
									<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } else { ?>
			<div class="products__wrapper">
				<div class="cultures__row">
					<h3><?php _e('No products in categories', 'dynayskyi'); ?></h3>
				</div>
			</div>
		<?php }
		wp_reset_postdata();
		wp_die();
	}

    public function __return_false() {
        return false;
    }
}

$ThemeSettingsClass = new ThemeSettingsClass();
<?php
/**
 * @package WordPress
 * @subpackage Dynayskyi
 * @since 1.0
 * @version 1.0
 */
get_header(); 
$posts_per_page = get_option('posts_per_page');
$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
$tags = get_tags();
$blog_id = get_option('page_for_posts');
$current_tag = get_queried_object();
?>
<section class="dns-blog__page">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="dns-page__title text-center">
                    <h1><?php _e('Blog', 'dynayskyi'); ?></h1>
                </div>
            </div>
        </div>
        <?php 
        if( $tags ){ ?>
        <div class="row">
            <div class="col">
                <div class="dns-tags__nav">
                    <nav>
                        <ul>
                            <li><a href="<?php echo get_permalink($blog_id); ?>"><?php _e('all', 'dynayskyi'); ?></a></li>
                            <?php 
                            foreach ( $tags as $tag ) {
                                $tag_link = get_term_link( $tag->term_id ); ?>
                                <li>
                                    <a href="<?php echo $tag_link; ?>"<?php echo $tag->term_id == $current_tag->term_id ? ' class="active"': ''; ?>><?php echo $tag->name; ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <?php }
        $args = array(
            'post_type'         => 'post',
            'posts_per_page'    => $posts_per_page,
            'post_status'       => 'publish',
            'paged'             => $paged,
            'tag_id'            => $current_tag->term_id
        );
        $query = new WP_Query( $args ); 
        if ( $query->have_posts() ) { ?>
        <div class="row">
            <?php while ( $query->have_posts() ) { $query->the_post(); ?>
            <div class="col-md-3">
                <?php get_template_part( 'template-parts/post/content', 'post-small' ); ?>
            </div>
            <?php } ?>
        </div>
        <?php } wp_reset_postdata(); 
        if( $query->max_num_pages > 1 ) { ?>
        <div class="row">
            <div class="col">
                <div class="dns-pagination text-center">
                <?php echo paginate_links( array(
                    'format'            => 'page/%#%',
                    'current'           => $paged,
                    'total'             => $query->max_num_pages,
                    'mid_size'          => 2,
                    'prev_next'         => true,
                    'prev_text'         => '<span></span>',
                    'next_text'         => '<span></span>',
                    'show_all'          => true
                )); ?>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</section>
<?php get_footer();
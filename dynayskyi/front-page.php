<?php
/**
 * @package WordPress
 * @subpackage Dynayskyi
 * @since 1.0
 * @version 1.0
 */

get_header(); 

if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'hero_section' ):
            get_template_part( 'template-parts/page/hero_section' );
        elseif( get_row_layout() == 'about_company_section' ): 
            get_template_part( 'template-parts/page/about_company_section' );
        elseif( get_row_layout() == 'our_values_section' ): 
            get_template_part( 'template-parts/page/our_values_section' );
        elseif( get_row_layout() == 'products_section' ): 
            get_template_part( 'template-parts/page/products_section' );
        elseif( get_row_layout() == 'blog_section' ): 
            get_template_part( 'template-parts/page/blog_section' );
        elseif( get_row_layout() == 'partners_section' ): 
            get_template_part( 'template-parts/page/partners_section' );
        endif;
    endwhile;
endif;

get_footer();
<?php
/**
 * @package WordPress
 * @subpackage Adamas
 * @since 1.0
 * @version 1.0
 */
?>  
    </main>
    <footer class="dns-footer scroll__section" id="contacts">
        <div class="container">
            <?php if( get_field('footer_title', 'option') ) { ?>
            <div class="row">
                <div class="col">
                    <div class="dns-footer__title">
                        <h3><?php the_field('footer_title', 'option'); ?></h3>
                    </div>
                </div>
            </div>
            <?php } ?> 
            <div class="row">
            <?php 
            $contacts = get_field('contacts_blocks', 'option');
            if( $contacts ) { ?>
                <div class="col-md-8">
                    <?php 
                    $i = 1;
                    $array_size = sizeof($contacts);
                    foreach ( $contacts as $contact ) { 
                        if( $i % 2 == 1 ) { ?>
                        <div class="row">
                        <?php } ?>
                            <div class="col-md-6">
                                <div class="dns-footer__contacts">
                                    <?php if( $contact['title'] ) { ?>
                                        <h3><?php echo $contact['title']; ?></h3>
                                    <?php } 
                                    echo $contact['contacts']; ?>
                                </div>
                            </div>
                        <?php if( $i % 2 != 1 || $i == $array_size ) { ?>
                        </div>
                        <?php } 
                    $i++; } ?>
                </div>
            <?php } ?>
                <div class="col-md-4">
                <?php if( get_field('footer_form_title', 'option') || get_field('form_button_label', 'option') ) { ?>
                    <div class="row">
                        <div class="col">
                            <div class="dns-footer__contacts">
                                <?php if( get_field('footer_form_title', 'option') ) { ?>
                                    <h3><?php echo get_field('footer_form_title', 'option'); ?></h3>
                                <?php } 
                                if( get_field('form_button_label', 'option') ) { ?>
                                    <button class="btn btn__white show__popup" id="request-popup"><?php echo get_field('form_button_label', 'option'); ?></button>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } 
                if( get_field('facebook', 'option') || get_field('instagram', 'option') || get_field('footer_logo', 'option') ) { ?>
                    <div class="row">
                        <div class="col">
                            <?php if( get_field('facebook', 'option') || get_field('instagram', 'option') ) { ?>
                            <div class="dns-social__block float-start">
                            <?php if( get_field('facebook', 'option') ) { ?>
                                <a class="facebook" target="_blank" href="<?php the_field('facebook', 'option'); ?>"></a>
                            <?php }
                            if( get_field('instagram', 'option') ) { ?>
                                <a class="instagram" target="_blank" href="<?php the_field('instagram', 'option'); ?>"></a>
                            <?php } ?>
                            </div>
                            <?php } 
                            if( get_field('footer_logo', 'option') ) { ?>
                                <a class="dns-footer__logo float-start" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                    <img src="<?php echo get_field('footer_logo', 'option')['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                </div>
            </div>
        </div>
    </footer>
    <?php wp_footer(); 
    if( get_field('form_shortcode', 'option') ) get_template_part( 'template-parts/form/request-popup' ); ?>
</body>
</html>